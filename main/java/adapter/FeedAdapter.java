package adapter;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import com.sun.syndication.feed.synd.SyndEntry;
import com.sun.syndication.feed.synd.SyndFeed;
import com.sun.syndication.fetcher.FeedFetcher;
import com.sun.syndication.fetcher.FetcherException;
import com.sun.syndication.fetcher.impl.HttpURLFeedFetcher;
import com.sun.syndication.io.FeedException;

import model.Article;
import model.Channel;

public class FeedAdapter {
	private String url;
	private SyndFeed feed;
	private MongoTemplate mongoTemplate;
	
	public FeedAdapter(String url, MongoTemplate mongoTemplate)
	{
		this.url = url;
		this.mongoTemplate = mongoTemplate;
		feed = null;
	}
	
	public String getChannelTitle(){
		return feed.getTitle();
	} 
	
	public String getChannelDescription(){
		return feed.getDescription();
	}
	
	public String getChannelLink(){
		return feed.getLink();
	}
	
	public List<Article> fetchArticles() {
        try {
            FeedFetcher feedFetcher = new HttpURLFeedFetcher();
            feed = feedFetcher.retrieveFeed(new URL(url));
            
            @SuppressWarnings("unchecked")
			List<SyndEntry> syndFeedItems = ((SyndFeed) feed).getEntries();
            List<Article> articles = new ArrayList<Article>();
            
            for (Object syndFeedEntry : syndFeedItems) {
                SyndEntry syndEntry = (SyndEntry) syndFeedEntry;
                //System.out.println(String.format("publish date: %s title: %s",syndEntry.getPublishedDate().toString(), syndEntry.getTitle()));
                Article article = new Article();
                article.setTitle(syndEntry.getTitle());
                article.setDescription(syndEntry.getDescription().getValue());
                article.setLink(syndEntry.getLink());
                article.setPublishDate(syndEntry.getPublishedDate());
                
                mongoTemplate.insert(article);
                articles.add(article);
            }
            System.out.println(String.format("Finished adding articles in DB.\nsize:%d",articles.size()));
            return articles;
            
        } catch (IllegalArgumentException e){
            System.out.println(String.format("error occurred: %s", e.getMessage()));
        } catch (IOException e){
            System.out.println(String.format("error occurred: %s", e.getMessage()));
        } catch (FeedException e){
            System.out.println(String.format("error occurred: %s", e.getMessage()));
        } catch (FetcherException e){
            System.out.println(String.format("error occurred: %s", e.getMessage()));
        }
        return null;
    }
	
	public void setUrl(String url){
		this.url = url;
	}

	/**
	 *  Retrieves channel information from given URL.
	 */
	public boolean fetchChannel(Channel channel) {
		try{
			FeedFetcher feedFetcher = new HttpURLFeedFetcher();
            feed = feedFetcher.retrieveFeed(new URL(url));

            channel.setDescription(feed.getDescription());
            channel.setTitle(feed.getTitle());
            
            return true;
		} catch (IllegalArgumentException e){
			System.out.println(String.format("error occurred: %s", e.getMessage()));
		} catch (IOException e){
			System.out.println(String.format("error occurred: %s", e.getMessage()));
		} catch (FeedException e){
			System.out.println(String.format("error occurred: %s", e.getMessage()));
		} catch (FetcherException e){
			System.out.println(String.format("error occurred: %s", e.getMessage()));
		}
		return false;
	}

	public List<Article> refresh() {
		try {
			FeedFetcher feedFetcher = new HttpURLFeedFetcher();
			feed = feedFetcher.retrieveFeed(new URL(url));
	            
	        @SuppressWarnings("unchecked")
	        List<SyndEntry> syndFeedItems = ((SyndFeed) feed).getEntries();
	        
	        List<Article> articles = new ArrayList<Article>();
	        
	        System.out.println(String.format("channel url is:%s", url));
	        
	        for (Object syndFeedEntry : syndFeedItems) {
	        	SyndEntry syndEntry = (SyndEntry) syndFeedEntry;
	                
	        	Query query = new Query(Criteria.where("link").is(syndEntry.getLink()));
	        	Article articleDB = mongoTemplate.findOne(query, Article.class);
	        	
	        	System.out.println(String.format("link is:%s",syndEntry.getLink()));
	        	
	        	if (articleDB == null){
	        		Article article = new Article();
	        		article.setTitle(syndEntry.getTitle());
	        		article.setDescription(syndEntry.getDescription().getValue());
	        		article.setLink(syndEntry.getLink());
	        		article.setPublishDate(syndEntry.getPublishedDate());
	        		mongoTemplate.insert(article);
	        		articles.add(article);
	        		System.out.println("Fresh articles are being added...!");
	        	}
	        }
	        return articles;
		} catch (IllegalArgumentException e){
			System.out.println(String.format("error occurred: %s", e.getMessage()));
	    } catch (IOException e){
	    	System.out.println(String.format("error occurred: %s", e.getMessage()));
	    } catch (FeedException e){
	    	System.out.println(String.format("error occurred: %s", e.getMessage()));
	    } catch (FetcherException e){
	    	System.out.println(String.format("error occurred: %s", e.getMessage()));
	    }
		return null;
	}
}
