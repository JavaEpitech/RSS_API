package model;

import java.util.Date;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import com.fasterxml.jackson.annotation.JsonProperty;

@Document(collection = "articles")
public class Article implements IFeed, Comparable<Article> {
	
	@JsonProperty
	private String title;
	
	@JsonProperty
    private String link;
	
	@JsonProperty
    private String description;

	@JsonProperty
	private boolean read;
	
	@Id
	private ObjectId _id;
	
    public Article(){
    }
    
    @DateTimeFormat(iso = ISO.DATE)
	private Date publishDate;
    
	@Override
	public String getTitle() {
		return title;
	}

	@Override
	public void setTitle(String title) {
		this.title = title;
	}

	@Override
	public String getLink() {
		return link;
	}

	@Override
	public void setLink(String link) {
		this.link = link;
	}

	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public void setDescription(String description) {
		if (description != null)
			this.description = description;
	}

	@Override
	public String getId() {
		return _id.toString();
	}

	@Override
	public void setId(ObjectId id) {
		this._id = id;
	}

	public boolean getRead() {
		return read;
	}

	public void setRead(boolean isRead) {
		this.read = isRead;
	}

	public Date getPublishDate() {
		return publishDate;
	}

	public void setPublishDate(Date publishDate) {
		this.publishDate = publishDate;
	}

	@Override
	public int compareTo(Article o) {
		if (this.publishDate != null && o.getPublishDate() != null)
		{
			if (publishDate.compareTo(o.getPublishDate()) > 0 && !read){
				return -1;
			}
			else{
				return 1;
			}
		}
		return 0;
	}
}
