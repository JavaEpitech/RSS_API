package model;

import java.util.ArrayList;
import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

@Document(collection = "feeds")
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public class Channel implements IFeed {

	@JsonProperty
    private String title;
	
	@JsonProperty
    private String link;
	
	@JsonProperty
    private String description;
	
	@JsonProperty
	private List<Article> items;
	
	@JsonProperty
	private boolean favorite;
	
	@Id
	private ObjectId _id;
	
	/*@DateTimeFormat(iso = ISO.DATE_TIME)
	private Date createdDate;*/

	public Channel(){
    	items = new ArrayList<Article>();
    }
    
	@Override
	public String getTitle() {
		return title;
	}

	@Override
	public void setTitle(String title) {
		this.title = title;
	}

	@Override
	public String getLink() {
		return link;
	}

	@Override
	public void setLink(String link) {
		this.link = link;
	}

	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String getId() {
		return _id.toString();
	}

	@Override
	public void setId(ObjectId id) {
		this._id = id;
	}
	
	public List<Article> getItems() {
			return items;
	}

	public void setItems(List<Article> items) {
			this.items = items;
	}

	public boolean getFavorite() {
		return favorite;
	}

	public void setFavorite(boolean isFavorite) {
		this.favorite = isFavorite;
	}
}
