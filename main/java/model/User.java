package model;

import java.util.ArrayList;
import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonProperty;

@Document(collection = "users")
public class User {
	
	@Id
	private ObjectId _id;
	
	@JsonProperty
    private String email;
	
	@JsonProperty
    private String password;
	
	@JsonProperty
	private List<ObjectId> feeds;
    
    public User(){
    	feeds = new ArrayList<ObjectId>();
    }
    
    public User(String email, String password){
		this.email = email;
        this.password = password;
        _id = ObjectId.get();
    }

    public String getEmail() {
        return email;
    }

    public void SetEmail(String email) {
    	this.email = email;
    }
    
    public String getPassword() {
        return password;
    }
    
    public void setPassword(String password) {
    	this.password = password;
    }
    
    public ObjectId getId() {
    	return _id;
    }
    
    public void setId(ObjectId id) {
    	this._id = id;
    }

	public List<ObjectId> getFeeds() {
		return feeds;
	}

	public void setFeeds(List<ObjectId> feeds) {
		this.feeds = feeds;
	}
}
