package model;

import org.bson.types.ObjectId;

/*
 * Channel interface.
 */
public interface IFeed {
	public String getTitle();
	public void setTitle(String title);
	public String getLink();
	public void setLink(String link);
	public String getDescription();
	public void setDescription(String description);
	public String getId();
	public void setId(ObjectId id);

}
