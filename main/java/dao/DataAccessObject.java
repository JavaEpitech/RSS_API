package dao;
import java.util.Collections;
import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import adapter.FeedAdapter;
import model.Article;
import model.Channel;
import model.User;

@Service
public class DataAccessObject {

	@Autowired
	private MongoTemplate mongoTemplate;
	
	/* 
	 *  Channel / Articles
	 */
	public boolean insert(Channel channel) {
		Channel channelQuery = mongoTemplate.findOne(new Query(Criteria.where("title").is(channel.getTitle())), Channel.class);
		if (channelQuery == null)
		{
			mongoTemplate.insert(channel);
			return true;
		}
		return false;
	}
	
	public List<Channel> getChannels(String userId, int page, int pageSize){
		//System.out.println(String.format("get channels - userId : %s", userId));
		List<ObjectId> channelRefArray = this.getChannelsFromUser(userId);
		Query query = new Query(Criteria.where("_id").in(channelRefArray));
		query.skip(pageSize * (page - 1));
		query.limit(pageSize);
		query.fields().exclude("items");
		List<Channel> result = mongoTemplate.find(query, Channel.class);
		if (result != null)
			return result;
		return null;
	}
	
	public List<Article> getArticles(String userId, int page, int pageSize, String channelId){
		List<ObjectId> channelRefArray = this.getChannelsFromUser(userId);
		Query query = new Query(Criteria.where("_id").in(channelRefArray));
		query.fields().include("items");
		List<Channel> channels = mongoTemplate.find(query, Channel.class);
				
		if (channels != null){
			for (Channel channel : channels){
				if (channel.getId().equalsIgnoreCase(channelId))
					System.out.println(String.format("get articles - size : %d", channel.getItems().size()));
					Collections.sort(channel.getItems());
					mongoTemplate.save(channel);
					return articlesPagination(channel.getItems(), page, pageSize);
			}
		}
		return null;
	}
	
	public boolean insert(Channel channel, String userId) {
		
		if (!isAlreadySaved(userId, channel.getLink()))
		{		
			// insert channel
			FeedAdapter feedFetcher = new FeedAdapter(channel.getLink(), mongoTemplate);
			if (!(feedFetcher.fetchChannel(channel)))
				return false;
			mongoTemplate.insert(channel);
			
			// Find channel ID and insert it as reference in the user's refArray
			ObjectId channelID = getChannelId(channel);
			User userDB = mongoTemplate.findOne(new Query(Criteria.where("_id").is(new ObjectId(userId))), User.class);
			userDB.getFeeds().add(channelID);
			userDB.setFeeds(userDB.getFeeds());
			mongoTemplate.save(userDB);
			
			// fetch the articles
			Channel channelDB = mongoTemplate.findOne(new Query(Criteria.where("_id").is(channelID)), Channel.class);
			List<Article> articles = feedFetcher.fetchArticles();
			if (articles == null)
				return false;
			channelDB.setItems(articles);
			mongoTemplate.save(channelDB);
			
			return true;
		}
		return false;
	}
	
	private boolean isAlreadySaved(String userId, String channelURL)
	{	
		List<ObjectId> channelRefArray = getChannelsFromUser(userId);
		if (channelRefArray == null)
			return false;
		List<Channel> channelsDB = mongoTemplate.find(new Query(Criteria.where("_id").in(channelRefArray)), Channel.class);
		if (channelsDB == null)
			return false;
		for (Channel channel : channelsDB){
			if (channel.getLink().equalsIgnoreCase(channelURL))
				return true;
		}
		return false;
	}
	
	private ObjectId getChannelId(Channel channel){
		Criteria channelCriteria = new Criteria().andOperator(Criteria.where("title").is(channel.getTitle()),  
															Criteria.where("link").is(channel.getLink()));
		Query query = new Query(channelCriteria);
		Channel channelDB = mongoTemplate.findOne(query, Channel.class);
		return new ObjectId(channelDB.getId());
	}
	
	public void setFavoriteChannel(String userID, String channelID, boolean isFavorite) {
		List<ObjectId> channelRefArray = this.getChannelsFromUser(userID);
		Query query = new Query(Criteria.where("_id").in(channelRefArray));
		List<Channel> channels = mongoTemplate.find(query, Channel.class);
		for (Channel channel : channels){
			if (channel.getId().equalsIgnoreCase(channelID)){
				channel.setFavorite(isFavorite);
				mongoTemplate.save(channel);
				break;
			}
		}
	}
	
	public void setArticleRead(String userID, String channelID, String articleID, boolean isRead) {
		List<ObjectId> channelRefArray = this.getChannelsFromUser(userID);
		Query query = new Query(Criteria.where("_id").in(channelRefArray));
		List<Channel> channels = mongoTemplate.find(query, Channel.class);
		for (Channel channel : channels){
			if (channel.getId().equalsIgnoreCase(channelID)){
				for (Article article : channel.getItems()){
					if (article.getId().equalsIgnoreCase(articleID)){
						article.setRead(isRead);
						mongoTemplate.save(channel);
						break;
					}
				}
				break;
			}
		}
	}
	
	public List<Article> refreshArticles(int page, int pageSize, String channelID) {
		
		Channel channelDB = mongoTemplate.findById(new ObjectId(channelID), Channel.class);
		
		if (channelDB == null)
			return null;

		FeedAdapter feedFetcher = new FeedAdapter(channelDB.getLink(), mongoTemplate);
		List<Article> freshArticles = feedFetcher.refresh();
		if (freshArticles.size() > 0)
		{
			System.out.println("adding new articles...");
			channelDB.getItems().addAll(freshArticles);
			
		}
				
		Collections.sort(channelDB.getItems());
		mongoTemplate.save(channelDB);
		return articlesPagination(channelDB.getItems(), page, pageSize);
	}
	
	public List<Article> removeChannel(String userID, String channelID) {
		return null;
	}
	
	/* 
	 *  User
	 */
	
	public boolean insert(User user) {
		User userQuery = mongoTemplate.findOne(new Query(Criteria.where("email").is(user.getEmail())), User.class);
		if (userQuery == null)
		{
			mongoTemplate.insert(user);
			return true;
		}
		return false;
	}

	public boolean delete(User user) {
		User userQuery = mongoTemplate.findOne(new Query(Criteria.where("email").is(user.getEmail())), User.class);
		if (userQuery != null)
		{
			mongoTemplate.remove(user);
			return true;
		}
		return false;
	}

	public User get(User user) {
		User userDB = mongoTemplate.findOne(new Query(Criteria.where("email").is(user.getEmail())), User.class);
		return userDB;
	}
	
	public ObjectId getId(User user){
		User userDB = mongoTemplate.findOne(new Query(Criteria.where("email").is(user.getEmail())), User.class);
		return userDB.getId();
	}
	
	public List<ObjectId> getChannelsFromUser(String userId){
	    Query channelRefArray = new Query(Criteria.where("_id").is(new ObjectId(userId)));
	    channelRefArray.fields().exclude("_id").include("feeds");
	    User user = mongoTemplate.findOne(channelRefArray, User.class);
	    if (user == null){
	    	System.out.println("user is null.");
	    	return null;
	    }
	    return user.getFeeds();
	}
	
	

	/*
	 * Helper methods 
	*/
	
	private List<Article> articlesPagination(List<Article> articles, int page, int pageSize){
		if (page <= 0 || pageSize <= 0 || articles == null){
			return articles;
		}
		if (page * pageSize > articles.size()){
			return articles;
		}
		int skip = pageSize * (page - 1);
		int limit = page * pageSize;
		return articles.subList(skip, limit);
	}


}