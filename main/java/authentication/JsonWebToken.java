package authentication;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;

import java.io.UnsupportedEncodingException;

import model.User;

public class JsonWebToken {
	
	private String secretKey = "secret30032017";
	private String token;
	private User user;
	
	public JsonWebToken(User user)
	{
		this.user = user;
		generateToken();
	}
	
	public JsonWebToken(String token)
	{
		this.token = token;
	}
	
	private void generateToken()
	{
		try {
			token = Jwts.builder()
					  .setSubject("users/TzMUocMF4p")
					  .setExpiration(null)
					  .claim("userId", user.getId().toString())
					  .claim("scope", "self groups/admins")
					  .signWith(SignatureAlgorithm.HS256, secretKey.getBytes("UTF-8")).compact();
			System.out.println(String.format("** USER ID = %s", user.getId().toString()));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}
	
	public String getToken()
	{
		return token;
	}
	
	public String extractUserId()
	{
		String jwt = token;
		Jws<Claims> claims = null;
		String userId = null;
		try {
			claims = Jwts.parser().setSigningKey(secretKey.getBytes("UTF-8")).parseClaimsJws(jwt);
			if (claims != null){
				userId = claims.getBody().get("userId").toString();
			}
		} catch (ExpiredJwtException e) {
			e.printStackTrace();
			return null;
		} catch (UnsupportedJwtException e) {
			e.printStackTrace();
			return null;
		} catch (MalformedJwtException e) {
			e.printStackTrace();
			return null;
		} catch (SignatureException e) {
			e.printStackTrace();
			return null;
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
			return null;
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			return null;
		}
		return userId;
	}
}