package controller;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import authentication.JsonWebToken;
import dao.DataAccessObject;
import model.User;

@RestController
public class UserController {
	
	@Autowired
	private DataAccessObject dao;
	
	@PostMapping(value = "/api/subscribe",
				consumes = "application/json",
				produces = "application/json")
	public ResponseEntity<Map<String, String>> createUser(@RequestBody User user)
	{
		if (dao.insert(user) == true){
			return new ResponseEntity<Map<String, String>>
			 (Collections.singletonMap("email", user.getEmail()), HttpStatus.CREATED);
		}
		return new ResponseEntity<Map<String, String>>
		 (Collections.singletonMap("error", "user exists"), 
				 HttpStatus.CONFLICT);
	}
	
	@PostMapping(value = "/api/login",
				consumes = "application/json",
				produces = "application/json")
	public ResponseEntity<Map<String, String>> loginUser(@RequestBody User user)
	{
		User userRecord = dao.get(user);
		if (userRecord != null){
			 if (userRecord.getPassword().equals(user.getPassword()))
			 {
				 JsonWebToken jwt = new JsonWebToken(userRecord);
				 Map<String, String> response = new HashMap<String, String>();
				 response.put("status", "authenticated");
				 response.put("auth_token", jwt.getToken());
				 return new ResponseEntity<Map<String, String>>
				 	(response, HttpStatus.OK);
			 }
			 return new ResponseEntity<Map<String, String>>
			 	(Collections.singletonMap("error", "incorrect username or password"), HttpStatus.UNAUTHORIZED);
		}
		return new ResponseEntity<Map<String, String>>
		 (Collections.singletonMap("status", "authentication failure"), HttpStatus.NOT_FOUND);
	}
}
