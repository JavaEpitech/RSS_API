package controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import authentication.JsonWebToken;
import dao.DataAccessObject;
import model.Article;
import model.Channel;

@RestController
public class ChannelController {
	
	@Autowired
	private DataAccessObject dao;
	
	/*
	 * Creates a channel/feed. 
	*/
	@PostMapping(value = "/api/feeds",
				consumes="application/json",
				produces = "application/json")
	public ResponseEntity<Map<String, String>> createChannel(@RequestBody Channel channel, 
												@RequestHeader("Authorization") String userToken)
	{
		JsonWebToken jwt = new JsonWebToken(userToken);
		String userID = null;
		if ((userID = jwt.extractUserId()) != null){
			if (dao.insert(channel, userID)){
				return new ResponseEntity<Map<String, String>>
					(Collections.singletonMap("status", "success"), HttpStatus.CREATED);
			}
			return new ResponseEntity<Map<String, String>>
			 (Collections.singletonMap("message", "You have already subscribed to that feed or"
			 		+ " there was a problem fetching a feed from given URL. "
			 		+ " Make sure the URL is valid and compliant with RSS 2.0."), HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<Map<String, String>>
		 (Collections.singletonMap("message", "invalid token"), HttpStatus.UNAUTHORIZED);
	}
	
	/*
	 * Get channels where page={page} with size of the page ={page_size}.
	*/
	@GetMapping(value = "/api/feeds",
				produces = "application/json")
	public ResponseEntity<List<Channel>> getChannels(@RequestParam(value="page", required=true) int page,
																@RequestParam(value="page_size", required=true) int pageSize,
																@RequestHeader("Authorization") String userToken)
	{
		JsonWebToken jwt = new JsonWebToken(userToken);
		String userID = null;
		if ((userID = jwt.extractUserId()) != null){
			List<Channel> channels = dao.getChannels(userID, page, pageSize);
			if (channels != null){
					return new ResponseEntity<List<Channel>>(channels, HttpStatus.OK);
			}
			return new ResponseEntity<List<Channel>>(new ArrayList<Channel>(), HttpStatus.UNAUTHORIZED);
		}
		return new ResponseEntity<List<Channel>>(new ArrayList<Channel>(), HttpStatus.BAD_REQUEST);
	}
	
	/*
	 * Get articles from channel where id of the channel = {id}.
	*/
	@GetMapping(value = "/api/feeds/{id}",
			produces = "application/json")
	public ResponseEntity<List<Article>> getPosts(@PathVariable(value="id") String channelId,
													@RequestParam(value="page", required=true) int page,
													@RequestParam(value="page_size", required=true) int pageSize,
													@RequestHeader("Authorization") String userToken)
	{
		JsonWebToken jwt = new JsonWebToken(userToken);
		String userID = null;
		if ((userID = jwt.extractUserId()) != null){
			List<Article> articles = dao.getArticles(userID, page, pageSize, channelId);
			if (articles != null){
					return new ResponseEntity<List<Article>>(articles, HttpStatus.OK);
			}
		}
		return new ResponseEntity<List<Article>>(new ArrayList<Article>(), HttpStatus.BAD_REQUEST);
	}

	/*
	 *	Sets the channel as favorite.
	 *
	*/
	@PostMapping(value = "/api/feeds/{id}/channel/fav",
			produces = "application/json")
	public ResponseEntity<Map<String, Boolean>> setFavorite(@PathVariable(value="id") String channelID,
															@RequestParam(value="favorite", required=true) boolean isFavorite,
															@RequestHeader("Authorization") String userToken)									
	{
		JsonWebToken jwt = new JsonWebToken(userToken);
		String userID = null;
		if ((userID = jwt.extractUserId()) != null){
			dao.setFavoriteChannel(userID, channelID, isFavorite);
			return new ResponseEntity<Map<String, Boolean>>
			(Collections.singletonMap("favorite", isFavorite), HttpStatus.OK);
		}
		return new ResponseEntity<Map<String, Boolean>>
		(Collections.singletonMap("status", false), HttpStatus.BAD_REQUEST);
	}
	
	/*
	 * Sets article as read or unread.
	 */
	@PostMapping(value = "/api/feeds/{id}/item/read",
			produces = "application/json")
	public ResponseEntity<Map<String, Boolean>> setRead(@PathVariable(value="id") String channelID,
															@RequestParam(value="read", required=true) boolean isRead,
															@RequestParam(value="article_id") String articleID,
															@RequestHeader("Authorization") String userToken)
															
	{
		JsonWebToken jwt = new JsonWebToken(userToken);
		String userID = null;
		if ((userID = jwt.extractUserId()) != null){
			dao.setArticleRead(userID, channelID, articleID, isRead);
			return new ResponseEntity<Map<String, Boolean>>
			(Collections.singletonMap("read", isRead), HttpStatus.OK);
		}
		return new ResponseEntity<Map<String, Boolean>>
		(Collections.singletonMap("status", false), HttpStatus.BAD_REQUEST);
	}
	
	/*
	 * Refresh: fetches the list of the most recently published articles
	 */
	@GetMapping(value = "/api/refresh/{id}",
			produces = "application/json")
	public ResponseEntity<List<Article>> refresh(@PathVariable(value="id") String channelId,
													@RequestParam(value="page", required=true) int page,
													@RequestParam(value="page_size", required=true) int pageSize,
													@RequestHeader("Authorization") String userToken)
	{
		JsonWebToken jwt = new JsonWebToken(userToken);
		if ((jwt.extractUserId()) != null){
			List<Article> articles = dao.refreshArticles(page, pageSize, channelId);
			if (articles != null){
					return new ResponseEntity<List<Article>>(articles, HttpStatus.OK);
			}
		}
		return new ResponseEntity<List<Article>>(new ArrayList<Article>(), HttpStatus.BAD_REQUEST);
	}
	
	/*
	 *  Unsubscribe from a channel 
	*/
	@GetMapping(value = "/api/feeds/{id}/unsubscribe",
			produces = "application/json")
	public ResponseEntity<Map<String, String>> unsubscribe(@PathVariable(value="id") String channelId,
													@RequestParam(value="page", required=true) int page,
													@RequestParam(value="page_size", required=true) int pageSize,
													@RequestHeader("Authorization") String userToken)
	{
		JsonWebToken jwt = new JsonWebToken(userToken);
		String userID = jwt.extractUserId();
		if (userID != null){
			List<Article> articles = dao.removeChannel(userID, channelId);
			if (articles != null){
					return new ResponseEntity<Map<String, String>>
					(Collections.singletonMap("status", "successfully unsubscribed"), HttpStatus.OK);
			}
		}
		return new ResponseEntity<Map<String, String>>
		 (Collections.singletonMap("error", "Can't find the channel with the ID specified in request."), HttpStatus.BAD_REQUEST);
	}
}